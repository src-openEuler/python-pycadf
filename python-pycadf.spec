
Name:           python-pycadf
Version:        3.1.1
Release:        2
Summary:        CADF Python module
License:        Apache-2.0
URL:            https://docs.openstack.org/pycadf/latest/
Source0:        https://files.pythonhosted.org/packages/83/5a/45506b000bf13fee4da64304aec169f9cddd0704add9a0339fd61f76a25c/pycadf-3.1.1.tar.gz
BuildArch:      noarch

%description
This library provides an auditing data model based on \
the Cloud Auditing Data Federation specification, primarily \
for use by OpenStack. The goal is to establish strict expectations \
about what auditors can expect from audit notifications.


%package -n python3-pycadf
Summary:        CADF Python module
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr

Requires:       python3-debtcollector >= 1.2.0
Requires:       python3-oslo-config 
Requires:       python3-oslo-serialization >= 2.18.0
Requires:       python3-pytz
Requires:       python3-six >= 1.10.0
Requires:       python-pycadf-common = %{version}-%{release}

%description -n python3-pycadf
This library provides an auditing data model based on \
the Cloud Auditing Data Federation specification, primarily \
for use by OpenStack. The goal is to establish strict expectations \
about what auditors can expect from audit notifications.

%package -n python-pycadf-common
Summary:        CADF Python module

%description -n python-pycadf-common
This library provides an auditing data model based on \
the Cloud Auditing Data Federation specification, primarily \
for use by OpenStack. The goal is to establish strict expectations \
about what auditors can expect from audit notifications.


%prep

%setup -q -n pycadf-3.1.1
rm -rf pycadf.egg-info

%build
%{py3_build}

%install
%{py3_install}

mkdir -p %{buildroot}/%{_sysconfdir}
mv %{buildroot}/usr/etc/pycadf %{buildroot}/%{_sysconfdir}/

%files -n python3-pycadf
%{python3_sitelib}/pycadf
%{python3_sitelib}/pycadf-3.1.1-py%{python3_version}.egg-info

%files -n python-pycadf-common
%doc README.rst
%license LICENSE
%dir %{_sysconfdir}/pycadf
%config(noreplace) %{_sysconfdir}/pycadf/*.conf


%changelog
* Wed Jul 13 2022 Chenyx <chenyixiong3@huawei.com> - 3.1.1-2
- License compliance rectification

* Wed Dec 30 2020 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated


